import React, { Component } from 'react';
import { TextField } from 'material-ui';
import styles from './styles.css';

export default class Login extends Component {

	constructor(context, props) {
		super(context, props);
	}

	render() {
		return(
			<div className={styles.login}>
				<div className={styles.login__main}>
					<div className={styles.login__main_form}>
						<TextField 
							className={styles.login__main_input}
							floatingLabelText="Login"/>
						<TextField 
							className={styles.login__main_input}
							floatingLabelText="Password"/>
					</div>
					<div className={styles.login__main_triangle} />	
				</div>				
				<div className={styles.login__backBlue}/>
			</div>
		);
	}

}