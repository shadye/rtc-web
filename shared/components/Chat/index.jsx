import React, { Component } from 'react';
import * as Actions from '../../redux/actions/actions';

//to append need <Chat dispatch={this.props.dispatch} peer={this.props.peer}/>
export default class Chat extends  Component {

	constructor(context, props) {
		super(context, props);
		this.state = {
	      showAddPost: false,
	      minutes: null,
	      hours: null,
	      message: '',
	      messages: [],
	      nickname: ''
	    };
	    this.setDate = this.setDate.bind(this);
	    this.handleInput = this.handleInput.bind(this);
	    this.sendMessage = this.sendMessage.bind(this);
	    this.appendMessage = this.appendMessage.bind(this);
	    this.onConnect = this.onConnect.bind(this);
	}

   handleInput(e) {
    var temp = {};
    temp[e.target.name] = e.target.value;
    this.setState(temp);
  }

	setDate() {
		var d = new Date();
		var m = d.getMinutes();
		if (this.state.minutes != m) {
		this.setState({ minutes: m });
		if (m < 10) m = '0'+m
			return <div className="timestamp">{`${d.getHours()}:${m}`}</div>
		}
	}

  appendMessage(message) {
    var history = this.state.messages;
    this.setState({ messages: [...history, message] });
  }

  sendMessage() {    
    const newMessage = <div className="message message-personal new">{this.state.message}{this.setDate()}</div>
    this.appendMessage(newMessage);
    this.setState({ message: '' });    
  }

  componentDidMount() {
    const { dispatch } = this.props;
    if (process.env.CLIENT) {
      const Peer = require('peerjs/lib/peer');

      const peer = new Peer({
        key: 'x7fwx2kavpy6tj4i',
        debug: 3
      });

      peer.on('open', (id) => { dispatch(Actions.setPeerId(id)) });
      peer.on('connection', this.onConnect);
      peer.on('error', (err) => { console.error(err) });
    }
  }

  onConnect(c) {
    if (c.label === 'chat') {
      c.on('data', (data) => {
        const incomingMessage = 
        <div class="message new">
          <figure class="avatar">
            <img src="http://s3-us-west-2.amazonaws.com/s.cdpn.io/156381/profile/profile-80_4.jpg" />
          </figure>
          {data}{this.setDate()}
        </div>
        this.appendMessage(incomingMessage);
      })
    }
  }



	render() {
		return(
      <div className="container">
        <div className="chat">
          <div className="chat-title">
            <h1><textarea   type="text" 
                            className="message-input"
                            name="nickname"
                            placeholder="your nickname..."
                            value={this.state.nickname}
                            onChange={this.handleInput}/></h1>
            <h2>{this.props.peer.id}</h2>
            <figure className="avatar">
              <img src="http://s3-us-west-2.amazonaws.com/s.cdpn.io/156381/profile/profile-80_4.jpg" /></figure>
          </div>
          <div className="messages">
            <div className="messages-content">
              {this.state.messages}
            </div>
          </div>
          <div className="message-box">
            <textarea type="text" className="message-input" name="message" placeholder="Type message..." value={this.state.message} onChange={this.handleInput}></textarea>
            <button type="submit" className="message-submit" onClick={this.sendMessage}>Send</button>
          </div>

        </div>
        <div className="bg"></div>
      </div>
		);
	}

}

Chat.need = [
  () => { return Actions.setPeerId(); }
];