import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import * as Actions from '../../redux/actions/actions';
import Chat from '../../components/Chat';
import Login from '../../components/Login';

import styles from './styles.css';

class Main extends Component {
  constructor(props, context) {
    super(props, context);
    
  }

  handleInput(e) {
    var temp = {};
    temp[e.target.name] = e.target.value;
    this.setState(temp);
  }

  render() {
    return (
      <div className={styles.app}>
        <Login/>
      </div>
    );
  }
}

Main.contextTypes = {
  router: React.PropTypes.object,
};

function mapStateToProps(store) {
  return {
    peer: store.peer
  };
}

Main.propTypes = {
  peer: PropTypes.objectOf(PropTypes.shape({
    id: PropTypes.string.isRequired
  })).isRequired,
  dispatch: PropTypes.func.isRequired,
};

export default connect(mapStateToProps)(Main);
